﻿Shader "Unlit/Progress"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_Progress("Progress", Range(0.0, 1.0)) = 0.3
		_OutlineXWidth("Outline X Width", Range(0.0, 0.05)) = 0.1
		_OutlineYWidth("Outline Y Width", Range(0.0, 0.2)) = 0.1
		_OutlineColor("Outline Color", Color) = (0,0,0)

		_GradientColorTop("Gradient Color Top", Color) = (0,0,0)
		_GradientColorBottom("Gradient Color Bottom", Color) = (0,0,0)
		_GradientColorSplit("Gradient Color Split", Range(0.0, 1.0)) = 0.1
		
		_Speed("Speed", Range(0.0, 20.0)) = 0.1
		_Amount("Amount", Range(0.0, 1.0)) = 0.1
		_Amplitude("Amplitude", Range(0.0, 20.0)) = 0.1

	}
	SubShader
	{
		Tags { "RenderType"="Opaque" }
		LOD 100

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			sampler2D _MainTex;
			float4 _MainTex_ST;
			fixed4 _OutlineColor;
			float _Progress;
			float _OutlineXWidth;
			float _OutlineYWidth;

			fixed4 _GradientColorTop;
			fixed4 _GradientColorBottom;
			float _GradientColorSplit;

			float _Amount;
			float _Speed;
			float _Amplitude;

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
			// branching is bad, mul is better than div : https://forum.unity3d.com/threads/how-to-measure-shader-performance.219519/
				float disp = (1+sin(_Time.y*_Speed + i.uv.y*_Amplitude)) * _Amount;
				if( i.uv.x > _Progress+disp) discard;
				if( i.uv.x > _Progress-_OutlineXWidth+disp ) return _OutlineColor;

				if( i.uv.y < _OutlineYWidth ) return _OutlineColor;
				if( i.uv.y > 1-_OutlineYWidth ) return _OutlineColor;

				if( i.uv.x < _OutlineXWidth ) return _OutlineColor;
				
				float height = 0.5-_OutlineYWidth;
				float split = 0.0;
				if( i.uv.y < 0.5 )
				    split = i.uv.y/0.5/_GradientColorSplit;
				else
				    split = (1-i.uv.y)/0.5/_GradientColorSplit;

				// sample the texture
				//fixed4 col = tex2D(_MainTex, i.uv);
				fixed4 col = _GradientColorBottom*split+_GradientColorTop*(1-split);


				return col;
			}
			ENDCG
		}
	}
}
